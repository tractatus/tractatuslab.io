### Wittgenstein Tractatus Network

On display [here](http://tractatus.gitlab.io/).

#### Compiling locally

First you need to run `npm install`

#### About

We compile a multilingual parallel corpus from different versions of Wittgenstein's *Tractatus Logico-Philosophicus*, including the original in German and translations into English, Spanish, French, and Russian. Using this corpus we compute a similarity measure between propositions and render a visual network of relations for different languages. The lengths of the edges are determined by the similarity between two propositions and the colors correspond to each major propositional group from 1 to 7.


A report is available [here](https://www.clarin-d.de/images/lt4dh/pdf/LT4DH10.pdf
). If you use this visualization tool for academic purposes, please cite:

```
	@InProceedings{bucur-nisioi:2016:LT4DH,
	  author    = {Bucur, Anca  and  Nisioi, Sergiu},
	  title     = {A Visual Representation of Wittgenstein's Tractatus Logico-Philosophicus},
	  booktitle = {Proceedings of the Workshop on Language Technology Resources and Tools for Digital Humanities (LT4DH)},
	  month     = {December},
	  year      = {2016},
	  address   = {Osaka, Japan},
	  publisher = {The COLING 2016 Organizing Committee},
	  pages     = {71--75},
	  url       = {http://aclweb.org/anthology/W16-4010}
	}
```

#### Related project
[Wittgenstein's Tractatus Concepts Network](http://wittgenstein-network.gitlab.io)